/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
    // 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
    // Обработчик событий - это некая функция, которая происходит после того, как произошло данное событие.
    
let inputPrice = document.getElementById("inputPrice");

function getPrice() {
    let div = document.createElement("div");
    document.body.prepend(div);
    div.classList.add("price-wrapper")
    let textPrice = document.createElement("span");
    textPrice.style.display = "inline-block";
    textPrice.innerText = `Текущая цена: ${inputPrice.value} $`;
    div.prepend(textPrice)
    let deleteIcon = document.createElement("span");
    deleteIcon.innerText = "x";
    textPrice.after(deleteIcon);
    textPrice.classList.add("text-price");
    deleteIcon.classList.add("delete-icon");
    inputPrice.style.color = "limegreen";
    deleteIcon.addEventListener('click', function () {
        deleteIcon.closest('div').remove()
        inputPrice.value = "";
        inputPrice.style.color = "black";
    })

}

function errorMessage() {
    let error = document.querySelector(".error");
    if (!error) {
        let error = document.createElement("span");
        document.body.append(error);
        error.classList.add("error")
        error.innerText = "Please enter correct price";
        inputPrice.classList.add("input-error");
        inputPrice.style.color = "red";
    }
}

function removeError() {
    let error = document.querySelector(".error");
    if (error) {
        error.remove();
        inputPrice.classList.remove("input-error");
    }
}

function isValidPrice() {
    if (inputPrice.value < 0 || !inputPrice.value) {
        errorMessage();
        return
    }
    getPrice();
    removeError()
}

inputPrice.addEventListener("blur", isValidPrice);